package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "pais")
@NamedQuery(name = "Pais.findAll", query = "SELECT p FROM Pais p")
public class Pais implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkpais;

	@Column(nullable = false, columnDefinition = "text")
	private String pais;

	@Column(nullable = false, columnDefinition = "text")
	private String sigla;

	@OneToMany(mappedBy = "pais", orphanRemoval = true)
	private List<Nacionalidade> nacionalidades;

	@OneToMany(mappedBy = "pais", orphanRemoval = true)
	private List<Uf> ufs;

	public Pais() {
	}

	public Integer getPkpais() {
		return this.pkpais;
	}

	public void setPkpais(Integer pkpais) {
		this.pkpais = pkpais;
	}

	public String getPais() {
		return this.pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public List<Nacionalidade> getNacionalidades() {
		return this.nacionalidades;
	}

	public void setNacionalidades(List<Nacionalidade> nacionalidades) {
		this.nacionalidades = nacionalidades;
	}

	public Nacionalidade addNacionalidade(Nacionalidade nacionalidade) {
		getNacionalidades().add(nacionalidade);
		nacionalidade.setPais(this);

		return nacionalidade;
	}

	public Nacionalidade removeNacionalidade(Nacionalidade nacionalidade) {
		getNacionalidades().remove(nacionalidade);
		nacionalidade.setPais(null);

		return nacionalidade;
	}

	public List<Uf> getUfs() {
		return this.ufs;
	}

	public void setUfs(List<Uf> ufs) {
		this.ufs = ufs;
	}

	public Uf addUf(Uf uf) {
		getUfs().add(uf);
		uf.setPais(this);

		return uf;
	}

	public Uf removeUf(Uf uf) {
		getUfs().remove(uf);
		uf.setPais(null);

		return uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkpais == null) ? 0 : pkpais.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pais other = (Pais) obj;
		if (pkpais == null) {
			if (other.pkpais != null)
				return false;
		} else if (!pkpais.equals(other.pkpais))
			return false;
		return true;
	}

}