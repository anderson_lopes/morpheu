package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "bairro")
@NamedQuery(name = "Bairro.findAll", query = "SELECT b FROM Bairro b")
public class Bairro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkbairro;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String bairro;

	@ManyToOne
	@JoinColumn(name = "fkmunicipio", nullable = false, referencedColumnName = "pkmunicipio")
	private Municipio municipio;

	@OneToMany(mappedBy = "bairro")
	private List<Endereco> enderecos;

	public Bairro() {
	}

	public Integer getPkbairro() {
		return this.pkbairro;
	}

	public void setPkbairro(Integer pkbairro) {
		this.pkbairro = pkbairro;
	}

	public String getBairro() {
		return this.bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public List<Endereco> getEnderecos() {
		return this.enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Endereco addEndereco(Endereco endereco) {
		getEnderecos().add(endereco);
		endereco.setBairro(this);

		return endereco;
	}

	public Endereco removeEndereco(Endereco endereco) {
		getEnderecos().remove(endereco);
		endereco.setBairro(null);

		return endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkbairro == null) ? 0 : pkbairro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bairro other = (Bairro) obj;
		if (pkbairro == null) {
			if (other.pkbairro != null)
				return false;
		} else if (!pkbairro.equals(other.pkbairro))
			return false;
		return true;
	}

}