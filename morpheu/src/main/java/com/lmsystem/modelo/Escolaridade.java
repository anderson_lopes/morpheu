package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "escolaridade")
@NamedQuery(name = "Escolaridade.findAll", query = "SELECT e FROM Escolaridade e")
public class Escolaridade implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkescolaridade;

	@Column(columnDefinition = "text")
	private String codescolaridade;

	@Column(nullable = false, columnDefinition = "text")
	private String escolaridade;

	public Escolaridade() {
	}

	public Integer getPkescolaridade() {
		return pkescolaridade;
	}

	public void setPkescolaridade(Integer pkescolaridade) {
		this.pkescolaridade = pkescolaridade;
	}

	public String getCodescolaridade() {
		return codescolaridade;
	}

	public void setCodescolaridade(String codescolaridade) {
		this.codescolaridade = codescolaridade;
	}

	public String getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkescolaridade == null) ? 0 : pkescolaridade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Escolaridade other = (Escolaridade) obj;
		if (pkescolaridade == null) {
			if (other.pkescolaridade != null)
				return false;
		} else if (!pkescolaridade.equals(other.pkescolaridade))
			return false;
		return true;
	}

}