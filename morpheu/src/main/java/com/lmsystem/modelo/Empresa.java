package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

@Entity
@Table(name = "empresa")
@NamedQuery(name = "Empresa.findAll", query = "SELECT e FROM Empresa e")
public class Empresa implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkempresa;

	@Column(columnDefinition = "text", unique = true)
	@NotBlank(message = "O nome da empresa deve ser preenchido")
	private String empresa;

	@Column(columnDefinition = "text", unique = true)
	@NotBlank(message = "A sigla deve ser preenchido")
	private String sigla;

	@Column(columnDefinition = "text", unique = true)
	@CNPJ(message = "CNPJ inválido.")
	@NotEmpty(message = "CNPJ deve ser preenchido.")
	private String cnpj;

	@Column(columnDefinition = "text")
	@NotEmpty(message = "Informe a secretaria responsável.")
	private String secretaria;

	@Column(columnDefinition = "text")
	@NotEmpty(message = "Informe o órgão da secretaria.")
	private String orgao;

	@ManyToOne
	@JoinColumn(name = "fkuf", nullable = false, referencedColumnName = "pkuf")
	private Uf uf;

	public Empresa() {

	}

	public Integer getPkempresa() {
		return pkempresa;
	}

	public void setPkempresa(Integer pkempresa) {
		this.pkempresa = pkempresa;
	}

	public String getEmpresa() {
		return empresa;
	}

	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getSecretaria() {
		return secretaria;
	}

	public void setSecretaria(String secretaria) {
		this.secretaria = secretaria;
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkempresa == null) ? 0 : pkempresa.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Empresa other = (Empresa) obj;
		if (pkempresa == null) {
			if (other.pkempresa != null)
				return false;
		} else if (!pkempresa.equals(other.pkempresa))
			return false;
		return true;
	}

}