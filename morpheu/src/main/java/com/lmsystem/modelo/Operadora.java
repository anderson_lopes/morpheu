package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "operadora")
@NamedQuery(name = "Operadora.findAll", query = "SELECT o FROM Operadora o")
public class Operadora implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkoperadora;

	@Column(nullable = false)
	private Integer codoperadora;

	private byte[] logo;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String operadora;

	@OneToMany(mappedBy = "operadora1", orphanRemoval = true)
	private List<Contato> contatos1;

	@OneToMany(mappedBy = "operadora2", orphanRemoval = true)
	private List<Contato> contatos2;

	public Operadora() {
	}

	public Integer getPkoperadora() {
		return this.pkoperadora;
	}

	public void setPkoperadora(Integer pkoperadora) {
		this.pkoperadora = pkoperadora;
	}

	public Integer getCodoperadora() {
		return this.codoperadora;
	}

	public void setCodoperadora(Integer codoperadora) {
		this.codoperadora = codoperadora;
	}

	public byte[] getLogo() {
		return this.logo;
	}

	public void setLogo(byte[] logo) {
		this.logo = logo;
	}

	public String getOperadora() {
		return this.operadora;
	}

	public void setOperadora(String operadora) {
		this.operadora = operadora;
	}

	public List<Contato> getContatos1() {
		return this.contatos1;
	}

	public void setContatos1(List<Contato> contatos1) {
		this.contatos1 = contatos1;
	}

	public Contato addContatos1(Contato contatos1) {
		getContatos1().add(contatos1);
		contatos1.setOperadora1(this);

		return contatos1;
	}

	public Contato removeContatos1(Contato contatos1) {
		getContatos1().remove(contatos1);
		contatos1.setOperadora1(null);

		return contatos1;
	}

	public List<Contato> getContatos2() {
		return this.contatos2;
	}

	public void setContatos2(List<Contato> contatos2) {
		this.contatos2 = contatos2;
	}

	public Contato addContatos2(Contato contatos2) {
		getContatos2().add(contatos2);
		contatos2.setOperadora2(this);

		return contatos2;
	}

	public Contato removeContatos2(Contato contatos2) {
		getContatos2().remove(contatos2);
		contatos2.setOperadora2(null);

		return contatos2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkoperadora == null) ? 0 : pkoperadora.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operadora other = (Operadora) obj;
		if (pkoperadora == null) {
			if (other.pkoperadora != null)
				return false;
		} else if (!pkoperadora.equals(other.pkoperadora))
			return false;
		return true;
	}

}