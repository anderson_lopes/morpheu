package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "unidade_produto")
@NamedQuery(name = "UnidadeProduto.findAll", query = "SELECT u FROM UnidadeProduto u")
public class UnidadeProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkunidade;

	@Column(nullable = false)
	private Integer fator;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String sigla;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String unidade;

	@OneToMany(mappedBy = "unidadeProduto", orphanRemoval = true)
	private List<Produto> produtos;

	public UnidadeProduto() {
	}

	public Integer getPkunidade() {
		return this.pkunidade;
	}

	public void setPkunidade(Integer pkunidade) {
		this.pkunidade = pkunidade;
	}

	public Integer getFator() {
		return this.fator;
	}

	public void setFator(Integer fator) {
		this.fator = fator;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getUnidade() {
		return this.unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public List<Produto> getProdutos() {
		return this.produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Produto addProduto(Produto produto) {
		getProdutos().add(produto);
		produto.setUnidadeProduto(this);

		return produto;
	}

	public Produto removeProduto(Produto produto) {
		getProdutos().remove(produto);
		produto.setUnidadeProduto(null);

		return produto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkunidade == null) ? 0 : pkunidade.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UnidadeProduto other = (UnidadeProduto) obj;
		if (pkunidade == null) {
			if (other.pkunidade != null)
				return false;
		} else if (!pkunidade.equals(other.pkunidade))
			return false;
		return true;
	}

}