package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "endereco")
@NamedQuery(name = "Endereco.findAll", query = "SELECT e FROM Endereco e")
public class Endereco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkendereco;

	@Column(columnDefinition = "text")
	private String complemento;

	@Column(columnDefinition = "text")
	private String endereco;

	@Column(columnDefinition = "text")
	private String numero;

	@ManyToOne
	@JoinColumn(name = "fkbairro", referencedColumnName = "pkbairro")
	private Bairro bairro;

	@ManyToOne
	@JoinColumn(name = "fkmunicipio", referencedColumnName = "pkmunicipio")
	private Municipio municipio;

	@ManyToOne
	@JoinColumn(name = "fkuf", referencedColumnName = "pkuf")
	private Uf uf;

	private String cep;

	@ManyToOne
	@JoinColumn(name = "fktipologradouro", referencedColumnName = "pktipologradouro")
	private TipoLogradouro tipoLogradouro;

	public Endereco() {
	}

	public Integer getPkendereco() {
		return this.pkendereco;
	}

	public void setPkendereco(Integer pkendereco) {
		this.pkendereco = pkendereco;
	}

	public String getComplemento() {
		return this.complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getEndereco() {
		return this.endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Bairro getBairro() {
		return this.bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public TipoLogradouro getTipoLogradouro() {
		return this.tipoLogradouro;
	}

	public void setTipoLogradouro(TipoLogradouro tipoLogradouro) {
		this.tipoLogradouro = tipoLogradouro;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Uf getUf() {
		return uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkendereco == null) ? 0 : pkendereco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Endereco other = (Endereco) obj;
		if (pkendereco == null) {
			if (other.pkendereco != null)
				return false;
		} else if (!pkendereco.equals(other.pkendereco))
			return false;
		return true;
	}

}