package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "contato")
@NamedQuery(name = "Contato.findAll", query = "SELECT c FROM Contato c")
public class Contato implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkcontato;

	@Column(columnDefinition = "text")
	private String celular1;

	@Column(columnDefinition = "text")
	private String celular2;

	@Column(columnDefinition = "text")
	private String comercial;

	@Column(columnDefinition = "text")
	private String funcionario;

	@Column(columnDefinition = "text")
	private String redesocial;

	@Column(columnDefinition = "text")
	private String website;

	@Email(message = "email inválido")
	@Column(columnDefinition = "text")
	private String email;

	@Column(columnDefinition = "text")
	private String residencial;

	@ManyToOne
	@JoinColumn(name = "fkoperadora1", referencedColumnName = "pkoperadora")
	private Operadora operadora1;

	@ManyToOne
	@JoinColumn(name = "fkoperadora2", referencedColumnName = "pkoperadora")
	private Operadora operadora2;

	@OneToMany(mappedBy = "contato", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	@OneToMany(mappedBy = "contato", orphanRemoval = true)
	private List<PessoaJuridica> pessoaJuridicas;

	public Contato() {
	}

	public Integer getPkcontato() {
		return this.pkcontato;
	}

	public void setPkcontato(Integer pkcontato) {
		this.pkcontato = pkcontato;
	}

	public String getCelular1() {
		return this.celular1;
	}

	public void setCelular1(String celular1) {
		this.celular1 = celular1;
	}

	public String getCelular2() {
		return this.celular2;
	}

	public void setCelular2(String celular2) {
		this.celular2 = celular2;
	}

	public String getComercial() {
		return this.comercial;
	}

	public void setComercial(String comercial) {
		this.comercial = comercial;
	}

	public String getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(String funcionario) {
		this.funcionario = funcionario;
	}

	public String getRedesocial() {
		return redesocial;
	}

	public void setRedesocial(String redesocial) {
		this.redesocial = redesocial;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getResidencial() {
		return this.residencial;
	}

	public void setResidencial(String residencial) {
		this.residencial = residencial;
	}

	public Operadora getOperadora1() {
		return this.operadora1;
	}

	public void setOperadora1(Operadora operadora1) {
		this.operadora1 = operadora1;
	}

	public Operadora getOperadora2() {
		return this.operadora2;
	}

	public void setOperadora2(Operadora operadora2) {
		this.operadora2 = operadora2;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setContato(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setContato(null);

		return pessoaFisica;
	}

	public List<PessoaJuridica> getPessoaJuridicas() {
		return this.pessoaJuridicas;
	}

	public void setPessoaJuridicas(List<PessoaJuridica> pessoaJuridicas) {
		this.pessoaJuridicas = pessoaJuridicas;
	}

	public PessoaJuridica addPessoaJuridica(PessoaJuridica pessoaJuridica) {
		getPessoaJuridicas().add(pessoaJuridica);
		pessoaJuridica.setContato(this);

		return pessoaJuridica;
	}

	public PessoaJuridica removePessoaJuridica(PessoaJuridica pessoaJuridica) {
		getPessoaJuridicas().remove(pessoaJuridica);
		pessoaJuridica.setContato(null);

		return pessoaJuridica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkcontato == null) ? 0 : pkcontato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contato other = (Contato) obj;
		if (pkcontato == null) {
			if (other.pkcontato != null)
				return false;
		} else if (!pkcontato.equals(other.pkcontato))
			return false;
		return true;
	}

}