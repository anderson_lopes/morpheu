package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "etnia")
@NamedQuery(name = "Etnia.findAll", query = "SELECT e FROM Etnia e")
public class Etnia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pketnia;

	@Column(nullable = false, columnDefinition = "text")
	private String etnia;

	@Column(nullable = false, columnDefinition = "text")
	private String codetnia;

	public Etnia() {
	}

	public Integer getPketnia() {
		return this.pketnia;
	}

	public void setPketnia(Integer pketnia) {
		this.pketnia = pketnia;
	}

	public String getEtnia() {
		return this.etnia;
	}

	public void setEtnia(String etnia) {
		this.etnia = etnia;
	}

	public String getCodetnia() {
		return codetnia;
	}

	public void setCodetnia(String codetnia) {
		this.codetnia = codetnia;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pketnia == null) ? 0 : pketnia.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Etnia other = (Etnia) obj;
		if (pketnia == null) {
			if (other.pketnia != null)
				return false;
		} else if (!pketnia.equals(other.pketnia))
			return false;
		return true;
	}

}