package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "municipio")
@NamedQuery(name = "Municipio.findAll", query = "SELECT m FROM Municipio m")
public class Municipio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkmunicipio;

	@Column(nullable = false, columnDefinition = "text")
	private String codmunicipio;

	@Column(nullable = false, columnDefinition = "text")
	private String municipio;

	@Column(nullable = false, columnDefinition = "text")
	private String gentilico;

	@OneToMany(mappedBy = "municipio", fetch = FetchType.EAGER, orphanRemoval = true)
	private List<Bairro> bairros;

	@ManyToOne
	@JoinColumn(name = "fkuf", nullable = false, referencedColumnName = "pkuf")
	private Uf uf;

	@OneToMany(mappedBy = "municipio", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	public Municipio() {
	}

	public Integer getPkmunicipio() {
		return this.pkmunicipio;
	}

	public void setPkmunicipio(Integer pkmunicipio) {
		this.pkmunicipio = pkmunicipio;
	}

	public String getCodmunicipio() {
		return this.codmunicipio;
	}

	public void setCodmunicipio(String codmunicipio) {
		this.codmunicipio = codmunicipio;
	}

	public String getMunicipio() {
		return this.municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getGentilico() {
		return gentilico;
	}

	public void setGentilico(String gentilico) {
		this.gentilico = gentilico;
	}

	public List<Bairro> getBairros() {
		return this.bairros;
	}

	public void setBairros(List<Bairro> bairros) {
		this.bairros = bairros;
	}

	public Bairro addBairro(Bairro bairro) {
		getBairros().add(bairro);
		bairro.setMunicipio(this);

		return bairro;
	}

	public Bairro removeBairro(Bairro bairro) {
		getBairros().remove(bairro);
		bairro.setMunicipio(null);

		return bairro;
	}

	public Uf getUf() {
		return this.uf;
	}

	public void setUf(Uf uf) {
		this.uf = uf;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setMunicipio(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setMunicipio(null);

		return pessoaFisica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkmunicipio == null) ? 0 : pkmunicipio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Municipio other = (Municipio) obj;
		if (pkmunicipio == null) {
			if (other.pkmunicipio != null)
				return false;
		} else if (!pkmunicipio.equals(other.pkmunicipio))
			return false;
		return true;
	}
}