package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "grupo_produto")
@NamedQuery(name = "GrupoProduto.findAll", query = "SELECT g FROM GrupoProduto g")
public class GrupoProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkgrupoproduto;

	@Column(nullable = false, columnDefinition = "text")
	private String grupoproduto;

	@OneToMany(mappedBy = "grupoProduto", orphanRemoval = true)
	private List<SubGrupoProduto> subgrupoProdutos;

	public GrupoProduto() {
	}

	public Integer getPkgrupoproduto() {
		return this.pkgrupoproduto;
	}

	public void setPkgrupoproduto(Integer pkgrupoproduto) {
		this.pkgrupoproduto = pkgrupoproduto;
	}

	public String getGrupoproduto() {
		return this.grupoproduto;
	}

	public void setGrupoproduto(String grupoproduto) {
		this.grupoproduto = grupoproduto;
	}

	public List<SubGrupoProduto> getSubgrupoProdutos() {
		return this.subgrupoProdutos;
	}

	public void setSubgrupoProdutos(List<SubGrupoProduto> subgrupoProdutos) {
		this.subgrupoProdutos = subgrupoProdutos;
	}

	public SubGrupoProduto addSubgrupoProduto(SubGrupoProduto subgrupoProduto) {
		getSubgrupoProdutos().add(subgrupoProduto);
		subgrupoProduto.setGrupoProduto(this);

		return subgrupoProduto;
	}

	public SubGrupoProduto removeSubgrupoProduto(SubGrupoProduto subgrupoProduto) {
		getSubgrupoProdutos().remove(subgrupoProduto);
		subgrupoProduto.setGrupoProduto(null);

		return subgrupoProduto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkgrupoproduto == null) ? 0 : pkgrupoproduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GrupoProduto other = (GrupoProduto) obj;
		if (pkgrupoproduto == null) {
			if (other.pkgrupoproduto != null)
				return false;
		} else if (!pkgrupoproduto.equals(other.pkgrupoproduto))
			return false;
		return true;
	}

}