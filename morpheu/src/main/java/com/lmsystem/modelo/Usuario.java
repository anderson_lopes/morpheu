package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "usuario")
@NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
public class Usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkusuario;

	@Column(unique = true, nullable = false, columnDefinition = "text")
	private String nome;

	@Column(unique = true, nullable = false, columnDefinition = "text")
	private String login;

	@Column(nullable = false, columnDefinition = "text")
	private String senha;

	@NotNull
	@Enumerated(EnumType.STRING)
	private Permissoes tipo;

	public Usuario() {
	}

	public Integer getPkusuario() {
		return pkusuario;
	}

	public void setPkusuario(Integer pkusuario) {
		this.pkusuario = pkusuario;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Permissoes getTipo() {
		return tipo;
	}

	public void setTipo(Permissoes tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkusuario == null) ? 0 : pkusuario.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (pkusuario == null) {
			if (other.pkusuario != null)
				return false;
		} else if (!pkusuario.equals(other.pkusuario))
			return false;
		return true;
	}

}