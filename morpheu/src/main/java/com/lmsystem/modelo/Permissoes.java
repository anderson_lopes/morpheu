package com.lmsystem.modelo;

public enum Permissoes {
	ADMINISTRADOR("Administrador"), GERENTE("Gerente"), USUSRIO_SIMPLES("Usuário simples");

	private String descricao;

	Permissoes(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}
}
