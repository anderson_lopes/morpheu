package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "estado_civil")
@NamedQuery(name = "EstadoCivil.findAll", query = "SELECT e FROM EstadoCivil e")
public class EstadoCivil implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkestadocivil;

	@Column(nullable = false, columnDefinition = "text")
	private String codestadocivil;

	@Column(nullable = false, columnDefinition = "text")
	private String estadocivil;

	public EstadoCivil() {
	}

	public Integer getPkestadocivil() {
		return this.pkestadocivil;
	}

	public void setPkestadocivil(Integer pkestadocivil) {
		this.pkestadocivil = pkestadocivil;
	}

	public String getCodestadocivil() {
		return this.codestadocivil;
	}

	public void setCodestadocivil(String codestadocivil) {
		this.codestadocivil = codestadocivil;
	}

	public String getEstadocivil() {
		return this.estadocivil;
	}

	public void setEstadocivil(String estadocivil) {
		this.estadocivil = estadocivil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkestadocivil == null) ? 0 : pkestadocivil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EstadoCivil other = (EstadoCivil) obj;
		if (pkestadocivil == null) {
			if (other.pkestadocivil != null)
				return false;
		} else if (!pkestadocivil.equals(other.pkestadocivil))
			return false;
		return true;
	}

}