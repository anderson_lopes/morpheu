package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Past;

import org.hibernate.validator.constraints.NotEmpty;
import org.hibernate.validator.constraints.br.CNPJ;

@Entity
@Table(name = "pessoa_juridica")
@NamedQuery(name = "PessoaJuridica.findAll", query = "SELECT p FROM PessoaJuridica p")
public class PessoaJuridica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkpessoajuridica;

	@Column(columnDefinition = "text", unique = true)
	@NotEmpty(message = "CNPJ deve ser preenchido.")
	@CNPJ(message = "CNPJ inválido.")
	private String cnpj;

	@Past
	@Temporal(value = TemporalType.DATE)
	private Date datafundacao;

	@Column(columnDefinition = "text")
	private String inscricaestadual;

	@Column(columnDefinition = "text")
	private String inscricamunicipal;

	@NotEmpty
	@Column(nullable = false, length = 80, columnDefinition = "text")
	private String nomefantasia;

	@NotEmpty
	@Column(nullable = false, length = 120, columnDefinition = "text")
	private String razaosocial;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkendereco", referencedColumnName = "pkendereco")
	private Endereco endereco;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkcontato", referencedColumnName = "pkcontato")
	private Contato contato;

	@ManyToOne
	@JoinColumn(name = "fkpessoajuridicadependente", referencedColumnName = "pkpessoajuridica")
	private PessoaJuridica pessoaJuridica;

	@ManyToOne
	@JoinColumn(name = "fkusuario", referencedColumnName = "pkusuario")
	private Usuario usuario;

	public PessoaJuridica() {
	}

	public Integer getPkpessoajuridica() {
		return this.pkpessoajuridica;
	}

	public void setPkpessoajuridica(Integer pkpessoajuridica) {
		this.pkpessoajuridica = pkpessoajuridica;
	}

	public String getCnpj() {
		return this.cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Date getDatafundacao() {
		return this.datafundacao;
	}

	public void setDatafundacao(Date datafundacao) {
		this.datafundacao = datafundacao;
	}

	public Endereco getEndereco() {
		return this.endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getInscricaestadual() {
		return this.inscricaestadual;
	}

	public void setInscricaestadual(String inscricaestadual) {
		this.inscricaestadual = inscricaestadual;
	}

	public String getInscricamunicipal() {
		return this.inscricamunicipal;
	}

	public void setInscricamunicipal(String inscricamunicipal) {
		this.inscricamunicipal = inscricamunicipal;
	}

	public String getNomefantasia() {
		return this.nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public String getRazaosocial() {
		return this.razaosocial;
	}

	public void setRazaosocial(String razaosocial) {
		this.razaosocial = razaosocial;
	}

	public Contato getContato() {
		return this.contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public PessoaJuridica getPessoaJuridica() {
		return this.pessoaJuridica;
	}

	public void setPessoaJuridica(PessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkpessoajuridica == null) ? 0 : pkpessoajuridica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaJuridica other = (PessoaJuridica) obj;
		if (pkpessoajuridica == null) {
			if (other.pkpessoajuridica != null)
				return false;
		} else if (!pkpessoajuridica.equals(other.pkpessoajuridica))
			return false;
		return true;
	}

}