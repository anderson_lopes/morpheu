package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import org.hibernate.validator.constraints.NotBlank;

@Entity
@Table(name = "tipopedido")
@NamedQuery(name = "TipoPedido.findAll", query = "SELECT t FROM TipoPedido t")
public class TipoPedido implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pktipopedido;

	@NotBlank
	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String tipopedido;

	@NotBlank
	@Column(nullable = false, columnDefinition = "text")
	private String tipo;

	public Integer getPktipopedido() {
		return pktipopedido;
	}

	public void setPktipopedido(Integer pktipopedido) {
		this.pktipopedido = pktipopedido;
	}

	public String getTipopedido() {
		return tipopedido;
	}

	public void setTipopedido(String tipopedido) {
		this.tipopedido = tipopedido;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pktipopedido == null) ? 0 : pktipopedido.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoPedido other = (TipoPedido) obj;
		if (pktipopedido == null) {
			if (other.pktipopedido != null)
				return false;
		} else if (!pktipopedido.equals(other.pktipopedido))
			return false;
		return true;
	}

}
