package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "produto")
@NamedQuery(name = "Produto.findAll", query = "SELECT p FROM Produto p")
public class Produto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkproduto;

	@Column(nullable = false)
	private double estoqueminimo;

	private byte[] foto;

	@Column(columnDefinition = "text")
	private String localizacao;

	private double precomedio;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String produto;

	@Column(nullable = false)
	private double ressuprimento;

	private double saldo;

	private double ultimopreco;

	@ManyToOne
	@JoinColumn(name = "fksubgrupo", nullable = false, referencedColumnName = "pksubgrupo")
	private SubGrupoProduto subgrupoProduto;

	@ManyToOne
	@JoinColumn(name = "fkunidadeestoque", nullable = false, referencedColumnName = "pkunidade")
	private UnidadeProduto unidadeProduto;

	public Produto() {
	}

	public Integer getPkproduto() {
		return this.pkproduto;
	}

	public void setPkproduto(Integer pkproduto) {
		this.pkproduto = pkproduto;
	}

	public double getEstoqueminimo() {
		return this.estoqueminimo;
	}

	public void setEstoqueminimo(double estoqueminimo) {
		this.estoqueminimo = estoqueminimo;
	}

	public byte[] getFoto() {
		return this.foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}

	public String getLocalizacao() {
		return this.localizacao;
	}

	public void setLocalizacao(String localizacao) {
		this.localizacao = localizacao;
	}

	public double getPrecomedio() {
		return this.precomedio;
	}

	public void setPrecomedio(double precomedio) {
		this.precomedio = precomedio;
	}

	public String getProduto() {
		return this.produto;
	}

	public void setProduto(String produto) {
		this.produto = produto;
	}

	public double getRessuprimento() {
		return this.ressuprimento;
	}

	public void setRessuprimento(double ressuprimento) {
		this.ressuprimento = ressuprimento;
	}

	public double getSaldo() {
		return this.saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public double getUltimopreco() {
		return this.ultimopreco;
	}

	public void setUltimopreco(double ultimopreco) {
		this.ultimopreco = ultimopreco;
	}

	public SubGrupoProduto getSubgrupoProduto() {
		return this.subgrupoProduto;
	}

	public void setSubgrupoProduto(SubGrupoProduto subgrupoProduto) {
		this.subgrupoProduto = subgrupoProduto;
	}

	public UnidadeProduto getUnidadeProduto() {
		return this.unidadeProduto;
	}

	public void setUnidadeProduto(UnidadeProduto unidadeProduto) {
		this.unidadeProduto = unidadeProduto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkproduto == null) ? 0 : pkproduto.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produto other = (Produto) obj;
		if (pkproduto == null) {
			if (other.pkproduto != null)
				return false;
		} else if (!pkproduto.equals(other.pkproduto))
			return false;
		return true;
	}

}