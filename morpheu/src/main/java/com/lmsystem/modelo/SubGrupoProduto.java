package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "subgrupo_produto")
@NamedQuery(name = "SubGrupoProduto.findAll", query = "SELECT s FROM SubGrupoProduto s")
public class SubGrupoProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pksubgrupo;

	@Column(nullable = false, columnDefinition = "text")
	private String subgrupo;

	@OneToMany(mappedBy = "subgrupoProduto", orphanRemoval = true)
	private List<Produto> produtos;

	@ManyToOne
	@JoinColumn(name = "fkgrupoproduto", nullable = false, referencedColumnName = "pkgrupoproduto")
	private GrupoProduto grupoProduto;

	public SubGrupoProduto() {
	}

	public Integer getPksubgrupo() {
		return this.pksubgrupo;
	}

	public void setPksubgrupo(Integer pksubgrupo) {
		this.pksubgrupo = pksubgrupo;
	}

	public String getSubgrupo() {
		return this.subgrupo;
	}

	public void setSubgrupo(String subgrupo) {
		this.subgrupo = subgrupo;
	}

	public List<Produto> getProdutos() {
		return this.produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public Produto addProduto(Produto produto) {
		getProdutos().add(produto);
		produto.setSubgrupoProduto(this);

		return produto;
	}

	public Produto removeProduto(Produto produto) {
		getProdutos().remove(produto);
		produto.setSubgrupoProduto(null);

		return produto;
	}

	public GrupoProduto getGrupoProduto() {
		return this.grupoProduto;
	}

	public void setGrupoProduto(GrupoProduto grupoProduto) {
		this.grupoProduto = grupoProduto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pksubgrupo == null) ? 0 : pksubgrupo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SubGrupoProduto other = (SubGrupoProduto) obj;
		if (pksubgrupo == null) {
			if (other.pksubgrupo != null)
				return false;
		} else if (!pksubgrupo.equals(other.pksubgrupo))
			return false;
		return true;
	}

}