package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "setor")
@NamedQuery(name = "Setor.findAll", query = "SELECT s FROM Setor s")
public class Setor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pksetor;

	@Column(nullable = false, columnDefinition = "text")
	private String setor;

	@ManyToOne
	@JoinColumn(name = "fksetordependente", referencedColumnName = "pksetor")
	private Setor setorBean;

	@OneToMany(mappedBy = "setorBean", orphanRemoval = true)
	private List<Setor> setors;

	public Setor() {
	}

	public Integer getPksetor() {
		return this.pksetor;
	}

	public void setPksetor(Integer pksetor) {
		this.pksetor = pksetor;
	}

	public String getSetor() {
		return this.setor;
	}

	public void setSetor(String setor) {
		this.setor = setor;
	}

	public Setor getSetorBean() {
		return this.setorBean;
	}

	public void setSetorBean(Setor setorBean) {
		this.setorBean = setorBean;
	}

	public List<Setor> getSetors() {
		return this.setors;
	}

	public void setSetors(List<Setor> setors) {
		this.setors = setors;
	}

	public Setor addSetor(Setor setor) {
		getSetors().add(setor);
		setor.setSetorBean(this);

		return setor;
	}

	public Setor removeSetor(Setor setor) {
		getSetors().remove(setor);
		setor.setSetorBean(null);

		return setor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pksetor == null) ? 0 : pksetor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Setor other = (Setor) obj;
		if (pksetor == null) {
			if (other.pksetor != null)
				return false;
		} else if (!pksetor.equals(other.pksetor))
			return false;
		return true;
	}

}