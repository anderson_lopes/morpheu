package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_logradouro")
@NamedQuery(name = "TipoLogradouro.findAll", query = "SELECT t FROM TipoLogradouro t")
public class TipoLogradouro implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pktipologradouro;

	@Column(nullable = false, columnDefinition = "text")
	private String codtipologradouro;

	@Column(nullable = false, columnDefinition = "text")
	private String tipologradouro;

	@OneToMany(mappedBy = "tipoLogradouro", orphanRemoval = true)
	private List<Endereco> enderecos;

	public TipoLogradouro() {
	}

	public Integer getPktipologradouro() {
		return this.pktipologradouro;
	}

	public void setPktipologradouro(Integer pktipologradouro) {
		this.pktipologradouro = pktipologradouro;
	}

	public String getCodtipologradouro() {
		return this.codtipologradouro;
	}

	public void setCodtipologradouro(String codtipologradouro) {
		this.codtipologradouro = codtipologradouro;
	}

	public String getTipologradouro() {
		return this.tipologradouro;
	}

	public void setTipologradouro(String tipologradouro) {
		this.tipologradouro = tipologradouro;
	}

	public List<Endereco> getEnderecos() {
		return this.enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public Endereco addEndereco(Endereco endereco) {
		getEnderecos().add(endereco);
		endereco.setTipoLogradouro(this);

		return endereco;
	}

	public Endereco removeEndereco(Endereco endereco) {
		getEnderecos().remove(endereco);
		endereco.setTipoLogradouro(null);

		return endereco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pktipologradouro == null) ? 0 : pktipologradouro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TipoLogradouro other = (TipoLogradouro) obj;
		if (pktipologradouro == null) {
			if (other.pktipologradouro != null)
				return false;
		} else if (!pktipologradouro.equals(other.pktipologradouro))
			return false;
		return true;
	}

}