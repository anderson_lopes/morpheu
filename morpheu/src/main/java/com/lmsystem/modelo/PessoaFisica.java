package com.lmsystem.modelo;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.validator.constraints.br.CPF;

@Entity
@Table(name = "pessoa_fisica")
@NamedQuery(name = "PessoaFisica.findAll", query = "SELECT p FROM PessoaFisica p")
public class PessoaFisica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkpessoafisica;

	@Column(columnDefinition = "text")
	private String apelido;

	@Column(columnDefinition = "text")
	private String cns;

	@CPF(message = "CPF possui um valor inválido.")
	@Column(columnDefinition = "text")
	private String cpf;

	@Temporal(value = TemporalType.DATE)
	@Column(nullable = false)
	private Date datanascimento;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkendereco", referencedColumnName = "pkendereco")
	private Endereco endereco;

	@Column(nullable = false, columnDefinition = "text")
	private String mae;

	@Column(nullable = false, columnDefinition = "text")
	private String nome;

	@Column(columnDefinition = "text")
	private String numerodocumento;

	@Column(columnDefinition = "text")
	private String pai;

	@Column(columnDefinition = "text")
	private String tituloeleitoral;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fkcontato", referencedColumnName = "pkcontato")
	private Contato contato;

	@ManyToOne
	@JoinColumn(name = "fkestadocivil", referencedColumnName = "pkestadocivil")
	private EstadoCivil estadoCivil;

	@ManyToOne
	@JoinColumn(name = "fketnia", referencedColumnName = "pketnia")
	private Etnia etnia;

	@ManyToOne
	@JoinColumn(name = "fknaturalidade", referencedColumnName = "pkmunicipio")
	private Municipio municipio;

	@ManyToOne
	@JoinColumn(name = "fknacionalidade", referencedColumnName = "pknacionalidade")
	private Nacionalidade nacionalidade;

	@ManyToOne
	@JoinColumn(name = "fkdependente", referencedColumnName = "pkpessoafisica")
	private PessoaFisica pessoaFisica;

	@OneToMany(mappedBy = "pessoaFisica", orphanRemoval = true)
	private List<PessoaFisica> pessoaFisicas;

	@ManyToOne
	@JoinColumn(name = "fkracacor", nullable = false, referencedColumnName = "pkracacor")
	private RacaCor racaCor;

	@ManyToOne
	@JoinColumn(name = "fksexo", nullable = false, referencedColumnName = "pksexo")
	private Sexo sexo;

	@ManyToOne
	@JoinColumn(name = "fktipodocumento", referencedColumnName = "pktipodocumento")
	private TipoDocumento tipoDocumento;

	@ManyToOne
	@JoinColumn(name = "fkusuario", referencedColumnName = "pkusuario")
	private Usuario usuario;

	@ManyToOne
	@JoinColumn(name = "fkocupacao", referencedColumnName = "pkocupacao")
	private Ocupacao ocupacao;

	public PessoaFisica() {
	}

	public Integer getPkpessoafisica() {
		return this.pkpessoafisica;
	}

	public void setPkpessoafisica(Integer pkpessoafisica) {
		this.pkpessoafisica = pkpessoafisica;
	}

	public String getApelido() {
		return this.apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}

	public String getCns() {
		return this.cns;
	}

	public void setCns(String cns) {
		this.cns = cns;
	}

	public String getCpf() {
		return this.cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Date getDatanascimento() {
		return this.datanascimento;
	}

	public void setDatanascimento(Date datanascimento) {
		this.datanascimento = datanascimento;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getMae() {
		return this.mae;
	}

	public void setMae(String mae) {
		this.mae = mae;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNumerodocumento() {
		return this.numerodocumento;
	}

	public void setNumerodocumento(String numerodocumento) {
		this.numerodocumento = numerodocumento;
	}

	public String getPai() {
		return this.pai;
	}

	public void setPai(String pai) {
		this.pai = pai;
	}

	public String getTituloeleitoral() {
		return this.tituloeleitoral;
	}

	public void setTituloeleitoral(String tituloeleitoral) {
		this.tituloeleitoral = tituloeleitoral;
	}

	public Contato getContato() {
		return this.contato;
	}

	public void setContato(Contato contato) {
		this.contato = contato;
	}

	public EstadoCivil getEstadoCivil() {
		return this.estadoCivil;
	}

	public void setEstadoCivil(EstadoCivil estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public Etnia getEtnia() {
		return this.etnia;
	}

	public void setEtnia(Etnia etnia) {
		this.etnia = etnia;
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public Nacionalidade getNacionalidade() {
		return this.nacionalidade;
	}

	public void setNacionalidade(Nacionalidade nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public PessoaFisica getPessoaFisica() {
		return this.pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public List<PessoaFisica> getPessoaFisicas() {
		return this.pessoaFisicas;
	}

	public void setPessoaFisicas(List<PessoaFisica> pessoaFisicas) {
		this.pessoaFisicas = pessoaFisicas;
	}

	public PessoaFisica addPessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().add(pessoaFisica);
		pessoaFisica.setPessoaFisica(this);

		return pessoaFisica;
	}

	public PessoaFisica removePessoaFisica(PessoaFisica pessoaFisica) {
		getPessoaFisicas().remove(pessoaFisica);
		pessoaFisica.setPessoaFisica(null);

		return pessoaFisica;
	}

	public RacaCor getRacaCor() {
		return this.racaCor;
	}

	public void setRacaCor(RacaCor racaCor) {
		this.racaCor = racaCor;
	}

	public Sexo getSexo() {
		return this.sexo;
	}

	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}

	public TipoDocumento getTipoDocumento() {
		return this.tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Ocupacao getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(Ocupacao ocupacao) {
		this.ocupacao = ocupacao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkpessoafisica == null) ? 0 : pkpessoafisica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PessoaFisica other = (PessoaFisica) obj;
		if (pkpessoafisica == null) {
			if (other.pkpessoafisica != null)
				return false;
		} else if (!pkpessoafisica.equals(other.pkpessoafisica))
			return false;
		return true;
	}

}