package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "banco")
@NamedQuery(name = "Banco.findAll", query = "SELECT b FROM Banco b")
public class Banco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkbanco;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String banco;

	@Column(columnDefinition = "text", unique = true, nullable = false)
	private String codfebraban;

	public Banco() {
	}

	public Integer getPkbanco() {
		return this.pkbanco;
	}

	public void setPkbanco(Integer pkbanco) {
		this.pkbanco = pkbanco;
	}

	public String getBanco() {
		return this.banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getCodfebraban() {
		return this.codfebraban;
	}

	public void setCodfebraban(String codfebraban) {
		this.codfebraban = codfebraban;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkbanco == null) ? 0 : pkbanco.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Banco other = (Banco) obj;
		if (pkbanco == null) {
			if (other.pkbanco != null)
				return false;
		} else if (!pkbanco.equals(other.pkbanco))
			return false;
		return true;
	}

}