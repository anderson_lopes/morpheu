package com.lmsystem.modelo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "ocupacao")
@NamedQuery(name = "Ocupacao.findAll", query = "SELECT o FROM Ocupacao o")
public class Ocupacao implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Integer pkocupacao;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String ocupacao;

	@Column(nullable = false, columnDefinition = "text", unique = true)
	private String cbo;

	public Ocupacao() {

	}

	public Integer getPkocupacao() {
		return pkocupacao;
	}

	public void setPkocupacao(Integer pkocupacao) {
		this.pkocupacao = pkocupacao;
	}

	public String getOcupacao() {
		return ocupacao;
	}

	public void setOcupacao(String ocupacao) {
		this.ocupacao = ocupacao;
	}

	public String getCbo() {
		return cbo;
	}

	public void setCbo(String cbo) {
		this.cbo = cbo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pkocupacao == null) ? 0 : pkocupacao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ocupacao other = (Ocupacao) obj;
		if (pkocupacao == null) {
			if (other.pkocupacao != null)
				return false;
		} else if (!pkocupacao.equals(other.pkocupacao))
			return false;
		return true;
	}

}
