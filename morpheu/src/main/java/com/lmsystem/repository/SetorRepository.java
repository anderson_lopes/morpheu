package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Setor;

public class SetorRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Setor obterPorID(Integer pksetor) {
		return manager.find(Setor.class, pksetor);
	}

	public Setor guardar(Setor entity) {
		return manager.merge(entity);
	}

	public void remover(Setor entity) {
		manager.remove(manager.getReference(Setor.class, entity.getPksetor()));
	}

	public List<Setor> listarTodos() {
		return manager.createQuery("from Setor order by pksetor desc", Setor.class).getResultList();
	}

	public List<Setor> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Setor where " + campo + " like :value", Setor.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Setor> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Setor where " + campo + " = :value", Setor.class).setParameter("value", value)
				.getResultList();
	}

}
