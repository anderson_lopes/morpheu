package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.SubGrupoProduto;

public class SubGrupoProdutoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public SubGrupoProduto obterPorID(Integer pksubgrupo) {
		return manager.find(SubGrupoProduto.class, pksubgrupo);
	}

	public SubGrupoProduto guardar(SubGrupoProduto entity) {
		return manager.merge(entity);
	}

	public void remover(SubGrupoProduto entity) {
		manager.remove(manager.getReference(SubGrupoProduto.class, entity.getPksubgrupo()));
	}

	public List<SubGrupoProduto> listarTodos() {
		return manager.createQuery("from SubGrupoProduto order by pksubgrupo desc", SubGrupoProduto.class)
				.getResultList();
	}

	public List<SubGrupoProduto> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from SubGrupoProduto where " + campo + " like :value order by pksubgrupo desc",
				SubGrupoProduto.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<SubGrupoProduto> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from SubGrupoProduto where " + campo + " = :value order by pksubgrupo desc",
				SubGrupoProduto.class).setParameter("value", value).getResultList();
	}

}
