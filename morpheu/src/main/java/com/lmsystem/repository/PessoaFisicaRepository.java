package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.PessoaFisica;

public class PessoaFisicaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public PessoaFisica obterPorID(long pk) {
		return manager.find(PessoaFisica.class, pk);
	}

	public PessoaFisica guardar(PessoaFisica entity) {
		return manager.merge(entity);
	}

	public void remover(PessoaFisica entity) {
		manager.remove(manager.getReference(PessoaFisica.class, entity.getPkpessoafisica()));
	}

	public List<PessoaFisica> listarTodos() {
		return manager.createQuery("from PessoaFisica order by pkpessoafisica desc", PessoaFisica.class)
				.getResultList();
	}

	public List<PessoaFisica> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from PessoaFisica where " + campo + " like :value order by pkpessoafisica desc",
				PessoaFisica.class).setParameter("value", "%" + value + "%").getResultList();
	}

	public List<PessoaFisica> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from PessoaFisica where " + campo + " = :value order by pkpessoafisica desc",
				PessoaFisica.class).setParameter("value", value).getResultList();
	}

	public List<PessoaFisica> buscarPorNomePessoa(String nomePessoa) {
		return manager.createQuery(
				"select p.pkpessoafisica, p.nome, p.datanascimento, p.mae, c.email, c.celular1 from PessoaFisica pf join fetch Contato c where p.nome like :value order by p.pkpessoafisica desc",
				PessoaFisica.class).setParameter("value", "%" + nomePessoa + "%").getResultList();
	}

}
