package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.EstadoCivil;

public class EstadoCivilRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public EstadoCivil obterPorID(Integer pkestadocivil) {
		return manager.find(EstadoCivil.class, pkestadocivil);
	}

	public EstadoCivil guardar(EstadoCivil entity) {
		return manager.merge(entity);
	}

	public void remover(EstadoCivil entity) {
		manager.remove(manager.getReference(EstadoCivil.class, entity.getPkestadocivil()));
	}

	public List<EstadoCivil> listarTodos() {
		return manager.createQuery("from EstadoCivil order by pkestadocivil desc", EstadoCivil.class).getResultList();
	}

	public List<EstadoCivil> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from EstadoCivil where " + campo + " like :value order by estadocivil", EstadoCivil.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<EstadoCivil> buscarPorRelacionamento(String campo, Object value) {
		return manager
				.createQuery("from EstadoCivil where " + campo + " = :value order by estadocivil", EstadoCivil.class)
				.setParameter("value", value).getResultList();
	}

}
