package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Escolaridade;

public class EscolaridadeRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Escolaridade obterPorID(Integer pkescolaridade) {
		return manager.find(Escolaridade.class, pkescolaridade);
	}

	public Escolaridade guardar(Escolaridade entity) {
		return manager.merge(entity);
	}

	public void remover(Escolaridade entity) {
		manager.remove(manager.getReference(Escolaridade.class, entity.getPkescolaridade()));
	}

	public List<Escolaridade> listarTodos() {
		return manager.createNamedQuery("Escolaridade.findAll", Escolaridade.class).getResultList();
	}

	public List<Escolaridade> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Escolaridade where " + campo + " like :value order by escolaridade",
				Escolaridade.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Escolaridade> buscarPorRelacionamento(String campo, Object value) {
		return manager
				.createQuery("from Escolaridade where " + campo + " = :value order by escolaridade", Escolaridade.class)
				.setParameter("value", value).getResultList();
	}

}
