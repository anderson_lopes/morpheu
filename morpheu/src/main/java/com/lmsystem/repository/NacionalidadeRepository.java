package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Nacionalidade;

public class NacionalidadeRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Nacionalidade obterPorID(Integer pknacionalidade) {
		return manager.find(Nacionalidade.class, pknacionalidade);
	}

	public Nacionalidade guardar(Nacionalidade entity) {
		return manager.merge(entity);
	}

	public void remover(Nacionalidade entity) {
		manager.remove(manager.getReference(Nacionalidade.class, entity.getPknacionalidade()));
	}

	public List<Nacionalidade> listarTodos() {
		return manager.createQuery("from Nacionalidade order by nacionalidade", Nacionalidade.class).getResultList();
	}

	public List<Nacionalidade> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Nacionalidade where " + campo + " like :value order by nacionalidade",
				Nacionalidade.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Nacionalidade> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Nacionalidade where " + campo + " = :value order by nacionalidade",
				Nacionalidade.class).setParameter("value", value).getResultList();
	}

}
