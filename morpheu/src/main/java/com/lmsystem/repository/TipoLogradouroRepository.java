package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.TipoLogradouro;

public class TipoLogradouroRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public TipoLogradouro obterPorID(Integer pktipologradouro) {
		return manager.find(TipoLogradouro.class, pktipologradouro);
	}

	public TipoLogradouro guardar(TipoLogradouro entity) {
		return manager.merge(entity);
	}

	public void remover(TipoLogradouro entity) {
		manager.remove(manager.getReference(TipoLogradouro.class, entity.getPktipologradouro()));
	}

	public List<TipoLogradouro> listarTodos() {
		return manager.createQuery("from TipoLogradouro order by tipologradouro", TipoLogradouro.class).getResultList();
	}

	public List<TipoLogradouro> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from TipoLogradouro where " + campo + " like :value order by tipologradouro",
				TipoLogradouro.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<TipoLogradouro> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from TipoLogradouro where " + campo + " = :value order by tipologradouro",
				TipoLogradouro.class).setParameter("value", value).getResultList();
	}
}
