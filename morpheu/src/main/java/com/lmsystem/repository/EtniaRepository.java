package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Etnia;

public class EtniaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Etnia obterPorID(Integer pketnia) {
		return manager.find(Etnia.class, pketnia);
	}

	public Etnia guardar(Etnia entity) {
		return manager.merge(entity);
	}

	public void remover(Etnia entity) {
		manager.remove(manager.getReference(Etnia.class, entity.getPketnia()));
	}

	public List<Etnia> listarTodos() {
		return manager.createQuery("from Etnia order by pketnia desc", Etnia.class).getResultList();
	}

	public List<Etnia> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Etnia where " + campo + " like :value order by etnia", Etnia.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Etnia> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Etnia where " + campo + " = :value order by etnia", Etnia.class)
				.setParameter("value", value).getResultList();
	}

}
