package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.TipoPedido;

public class TipoPedidoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public TipoPedido obterPorID(Integer pktipopedido) {
		return manager.find(TipoPedido.class, pktipopedido);
	}

	public TipoPedido guardar(TipoPedido entity) {
		return manager.merge(entity);
	}

	public void remover(TipoPedido entity) {
		manager.remove(manager.getReference(TipoPedido.class, entity.getPktipopedido()));
	}

	public List<TipoPedido> listarTodos() {
		return manager.createQuery("from TipoPedido order by pktipopedido desc", TipoPedido.class).getResultList();
	}

	public List<TipoPedido> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from TipoPedido where " + campo + " like :value order by tipopedido", TipoPedido.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<TipoPedido> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from TipoPedido where " + campo + " = :value order by tipopedido", TipoPedido.class)
				.setParameter("value", value).getResultList();
	}

}
