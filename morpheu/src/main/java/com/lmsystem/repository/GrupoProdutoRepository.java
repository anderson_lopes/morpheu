package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.GrupoProduto;

public class GrupoProdutoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public GrupoProduto obterPorID(Integer pkgrupoproduto) {
		return manager.find(GrupoProduto.class, pkgrupoproduto);
	}

	public GrupoProduto guardar(GrupoProduto entity) {
		return manager.merge(entity);
	}

	public void remover(GrupoProduto entity) {
		manager.remove(manager.getReference(GrupoProduto.class, entity.getPkgrupoproduto()));
	}

	public List<GrupoProduto> listarTodos() {
		return manager.createQuery("from GrupoProduto order by pkgrupoproduto desc", GrupoProduto.class)
				.getResultList();
	}

	public List<GrupoProduto> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from GrupoProduto where " + campo + " like :value order by pkgrupoproduto desc",
				GrupoProduto.class).setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

}
