package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Ocupacao;

public class OcupacaoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Ocupacao obterPorID(Integer pkocupacao) {
		return manager.find(Ocupacao.class, pkocupacao);
	}

	public Ocupacao guardar(Ocupacao entity) {
		return manager.merge(entity);
	}

	public void remover(Ocupacao entity) {
		manager.remove(manager.getReference(Ocupacao.class, entity.getPkocupacao()));
	}

	public List<Ocupacao> listarTodos() {
		return manager.createQuery("from Ocupacao order by pkocupacao desc", Ocupacao.class).getResultList();
	}

	public List<Ocupacao> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Ocupacao where " + campo + " like :value order by ocupacao", Ocupacao.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Ocupacao> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Ocupacao where " + campo + " = :value order by ocupacao", Ocupacao.class)
				.setParameter("value", value).getResultList();
	}

}