package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Produto;

public class ProdutoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Produto obterPorID(long pk) {
		return manager.find(Produto.class, pk);
	}

	public Produto guardar(Produto entity) {
		return manager.merge(entity);
	}

	public void remover(Produto entity) {
		manager.remove(manager.getReference(Produto.class, entity.getPkproduto()));
	}

	public List<Produto> listarTodos() {
		return manager.createQuery("from Produto order by pkproduto desc", Produto.class).getResultList();
	}

	public List<Produto> buscarPorDescricao(String campo, String value) {
		return manager
				.createQuery("from Produto where " + campo + " like :value order by pkproduto desc", Produto.class)
				.setParameter("value", "%" + value + "%").getResultList();
	}

	public List<Produto> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Produto where " + campo + " = :value order by pkproduto desc", Produto.class)
				.setParameter("value", value).getResultList();
	}

}
