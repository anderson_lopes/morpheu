package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Deposito;

public class DepositoRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Deposito obterPorID(Integer pkdeposito) {
		return manager.find(Deposito.class, pkdeposito);
	}

	public Deposito guardar(Deposito entity) {
		return manager.merge(entity);
	}

	public void remover(Deposito entity) {
		manager.remove(manager.getReference(Deposito.class, entity.getPkdeposito()));
	}

	public List<Deposito> listarTodos() {
		return manager.createQuery("from Deposito order by pkdeposito desc", Deposito.class).getResultList();
	}

	public List<Deposito> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Deposito where " + campo + " like :value", Deposito.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Deposito> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Deposito where " + campo + " = :value", Deposito.class)
				.setParameter("value", value).getResultList();
	}

}
