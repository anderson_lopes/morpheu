package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Bairro;

public class BairroRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Bairro obterPorID(Integer pkbairro) {
		return manager.find(Bairro.class, pkbairro);
	}

	public Bairro guardar(Bairro bairro) {
		return manager.merge(bairro);
	}

	public List<Bairro> listarTodos() {
		return manager.createQuery("from Bairro order by bairro", Bairro.class).getResultList();
	}

	public void remover(Bairro bairro) {
		manager.remove(manager.getReference(Bairro.class, bairro.getPkbairro()));
	}

	public List<Bairro> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Bairro where " + campo + " like :value order by bairro", Bairro.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Bairro> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Bairro where " + campo + " = :value order by bairro", Bairro.class)
				.setParameter("value", value).getResultList();
	}

}
