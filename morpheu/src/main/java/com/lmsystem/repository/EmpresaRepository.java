package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Empresa;

public class EmpresaRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Empresa obterPorID(Integer pkempresa) {
		return manager.find(Empresa.class, pkempresa);
	}

	public Empresa guardar(Empresa entity) {
		return manager.merge(entity);
	}

	public void remover(Empresa entity) {
		manager.remove(manager.getReference(Empresa.class, entity.getPkempresa()));
	}

	public List<Empresa> listarTodos() {
		return manager.createQuery("from Empresa order by pkempresa desc", Empresa.class).getResultList();
	}

	public List<Empresa> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Empresa where " + campo + " like :value", Empresa.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Empresa> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Empresa where " + campo + " = :value", Empresa.class)
				.setParameter("value", value).getResultList();
	}

}
