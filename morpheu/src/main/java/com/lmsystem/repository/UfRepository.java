package com.lmsystem.repository;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import com.lmsystem.modelo.Uf;

public class UfRepository implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager manager;

	public Uf obterPorID(Integer pkuf) {
		return manager.find(Uf.class, pkuf);
	}

	public Uf guardar(Uf entity) {
		return manager.merge(entity);
	}

	public void remover(Uf entity) {
		manager.remove(manager.getReference(Uf.class, entity.getPkuf()));
	}

	public List<Uf> listarTodos() {
		return manager.createQuery("from Uf order by pkUf desc", Uf.class).getResultList();
	}

	public List<Uf> buscarPorDescricao(String campo, String value) {
		return manager.createQuery("from Uf where " + campo + " like :value order by uf", Uf.class)
				.setParameter("value", "%" + value.toUpperCase() + "%").getResultList();
	}

	public List<Uf> buscarPorRelacionamento(String campo, Object value) {
		return manager.createQuery("from Uf where " + campo + " = :value order by uf", Uf.class)
				.setParameter("value", value).getResultList();
	}

}
