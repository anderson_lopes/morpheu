package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.SubGrupoProduto;
import com.lmsystem.repository.SubGrupoProdutoRepository;

@FacesConverter(forClass = SubGrupoProduto.class)
public class SubGrupoProdutoConverter implements Converter {

	@Inject
	private SubGrupoProdutoRepository subGrupoProdutoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return subGrupoProdutoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof SubGrupoProduto) {
			SubGrupoProduto subGrupoProduto = (SubGrupoProduto) value;
			return String.valueOf(subGrupoProduto.getPksubgrupo());
		}
		return "";
	}

}
