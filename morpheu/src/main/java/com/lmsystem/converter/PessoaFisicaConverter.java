package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.PessoaFisica;
import com.lmsystem.repository.PessoaFisicaRepository;

@FacesConverter(forClass = PessoaFisica.class)
public class PessoaFisicaConverter implements Converter {

	@Inject
	protected PessoaFisicaRepository pessoaFisicaRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return pessoaFisicaRepository.obterPorID(Long.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof PessoaFisica) {
			PessoaFisica pessoaFisica = (PessoaFisica) value;
			return String.valueOf(pessoaFisica.getPkpessoafisica());
		}
		return "";
	}

}
