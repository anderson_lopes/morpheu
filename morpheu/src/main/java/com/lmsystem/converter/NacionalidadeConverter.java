package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.repository.NacionalidadeRepository;

@FacesConverter(forClass = Nacionalidade.class)
public class NacionalidadeConverter implements Converter {

	@Inject
	protected NacionalidadeRepository nacionalidadeRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return nacionalidadeRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Nacionalidade) {
			Nacionalidade nacionalidade = (Nacionalidade) value;
			return String.valueOf(nacionalidade.getPknacionalidade());
		}
		return "";
	}

}
