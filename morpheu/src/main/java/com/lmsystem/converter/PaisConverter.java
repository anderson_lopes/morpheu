package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Pais;
import com.lmsystem.repository.PaisRepository;

@FacesConverter(forClass = Pais.class)
public class PaisConverter implements Converter {

	@Inject
	private PaisRepository paisRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return paisRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Pais) {
			Pais pais = (Pais) value;
			return String.valueOf(pais.getPkpais());
		}
		return "";
	}

}
