package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.GrupoProduto;
import com.lmsystem.repository.GrupoProdutoRepository;

@FacesConverter(forClass = GrupoProduto.class)
public class GrupoProdutoConverter implements Converter {

	@Inject
	private GrupoProdutoRepository grupoProdutoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return grupoProdutoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof GrupoProduto) {
			GrupoProduto grupoProduto = (GrupoProduto) value;
			return String.valueOf(grupoProduto.getPkgrupoproduto());
		}
		return "";
	}

}
