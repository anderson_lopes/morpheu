package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Deposito;
import com.lmsystem.repository.DepositoRepository;

@FacesConverter(forClass = Deposito.class)
public class DepositoConverter implements Converter {

	@Inject
	protected DepositoRepository depositoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return depositoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Deposito) {
			Deposito deposito = (Deposito) value;
			return String.valueOf(deposito.getPkdeposito());
		}
		return "";
	}

}
