package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.PessoaJuridica;
import com.lmsystem.repository.PessoaJuridicaRepository;

@FacesConverter(forClass = PessoaJuridica.class)
public class PessoaJuridicaConverter implements Converter {

	@Inject
	protected PessoaJuridicaRepository pessoaJuridicaRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return pessoaJuridicaRepository.obterPorID(Long.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof PessoaJuridica) {
			PessoaJuridica pessoaJuridica = (PessoaJuridica) value;
			return String.valueOf(pessoaJuridica.getPkpessoajuridica());
		}
		return "";
	}
}
