package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Banco;
import com.lmsystem.repository.BancoRepository;

@FacesConverter(forClass = Banco.class)
public class BancoConverter implements Converter {

	@Inject
	protected BancoRepository bancoRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return bancoRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Banco) {
			Banco banco = (Banco) value;
			return String.valueOf(banco.getPkbanco());
		}
		return "";
	}

}
