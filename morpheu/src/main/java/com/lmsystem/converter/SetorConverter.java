package com.lmsystem.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import com.lmsystem.modelo.Setor;
import com.lmsystem.repository.SetorRepository;

@FacesConverter(forClass = Setor.class)
public class SetorConverter implements Converter {

	@Inject
	private SetorRepository setorRepository;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		if (value != null && !value.equals("")) {
			return setorRepository.obterPorID(Integer.valueOf(value));
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object value) {
		if (value instanceof Setor) {
			Setor setor = (Setor) value;
			return String.valueOf(setor.getPksetor());
		}
		return "";
	}

}
