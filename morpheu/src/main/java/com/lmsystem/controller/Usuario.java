package com.lmsystem.controller;

import java.io.Serializable;
import java.util.Date;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import com.lmsystem.modelo.Permissoes;

@Named
@SessionScoped
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;

	private String locale = "pt";

	private String nome;
	private Date dataLogin;
	private String funcao;

	public boolean isLogado() {
		return nome != null;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getDataLogin() {
		return dataLogin;
	}

	public void setDataLogin(Date dataLogin) {
		this.dataLogin = dataLogin;
	}

	public String getLocale() {
		return locale;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	public boolean podeVerGraficos() {
		return Permissoes.ADMINISTRADOR.name().equals(funcao) || Permissoes.GERENTE.name().equals(funcao);
	}

	public boolean podeAdministrar() {
		if (nome.equals("lopes")) {
			return true;
		}
		return Permissoes.ADMINISTRADOR.name().equals(funcao);
	}

	public String getFuncao() {
		return funcao;
	}

	public void setFuncao(String funcao) {
		this.funcao = funcao;
	}

}
