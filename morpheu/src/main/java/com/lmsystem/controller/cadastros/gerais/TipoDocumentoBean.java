package com.lmsystem.controller.cadastros.gerais;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.TipoDocumento;
import com.lmsystem.services.cadastros.gerais.TipoDocumentoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class TipoDocumentoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private TipoDocumento tipoDocumento;

	private String nomeTipoDocumento;

	private List<TipoDocumento> tipoDocumentos;

	@Inject
	protected TipoDocumentoRN tipoDocumentoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeTipoDocumento("");
		pesquisar();
	}

	public void novo() {
		this.tipoDocumento = new TipoDocumento();
	}

	public void salvar() {
		try {
			tipoDocumentoRN.salvar(this.tipoDocumento);
			this.tipoDocumento = new TipoDocumento();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.tipoDocumento = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			tipoDocumentoRN.remover(this.tipoDocumento);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.tipoDocumento = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "tipo-documento?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/tipo-documento", opcoes, null);
	}

	public void pesquisar() {
		this.getTipoDocumentos();
	}

	public TipoDocumento getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(TipoDocumento tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getNomeTipoDocumento() {
		return nomeTipoDocumento;
	}

	public void setNomeTipoDocumento(String nomeTipoDocumento) {
		this.nomeTipoDocumento = nomeTipoDocumento;
	}

	public List<TipoDocumento> getTipoDocumentos() {
		this.tipoDocumentos = tipoDocumentoRN.listarTodos(getNomeTipoDocumento());
		return tipoDocumentos;
	}

}
