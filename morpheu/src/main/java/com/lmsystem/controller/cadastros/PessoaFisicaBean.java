package com.lmsystem.controller.cadastros;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Contato;
import com.lmsystem.modelo.Endereco;
import com.lmsystem.modelo.Etnia;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.PessoaFisica;
import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.Sexo;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.repository.BairroRepository;
import com.lmsystem.repository.MunicipioRepository;
import com.lmsystem.services.cadastros.PessoaFisicaRN;
import com.lmsystem.util.BuscaCEP;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class PessoaFisicaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private PessoaFisica pessoaFisica;

	private String nomePessoa;

	private List<PessoaFisica> pessoasFisica;

	@Inject
	protected PessoaFisicaRN pessoaRN;

	@Inject
	protected BairroRepository bairroRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomePessoa("");
		pesquisar();
	}

	public void novo() {
		this.pessoaFisica = new PessoaFisica();
		this.getPessoaFisica().setContato(new Contato());
		this.getPessoaFisica().setEndereco(new Endereco());
	}

	public void salvar() {
		try {
			pessoaRN.salvar(getPessoaFisica());
			pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.pessoaFisica = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			pessoaRN.remover(this.getPessoaFisica());
			this.pesquisar();
			facesUtils.exibeSucesso("registro removido com sucesso!", null);
			this.pessoaFisica = null;
			iniciar();
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
		}
	}

	public String limpar() {
		return "pessoafisica?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		opcoes.put("resizable", false);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/pessoafisica", opcoes, null);
	}

	public void pesquisar() {
		this.getPessoasFisicas();
	}

	public void buscarCEP() {
		BuscaCEP cep = new BuscaCEP();
		try {
			if (!this.getPessoaFisica().getEndereco().getCep().trim().equals("")
					|| this.getPessoaFisica().getEndereco().getCep() != null) {
				String tmp = cep.getCidade(this.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					this.getPessoaFisica().getEndereco()
							.setMunicipio(municipioRepository.buscarPorDescricao("municipio", tmp).get(0));
					tmp = null;
				}
				tmp = cep.getEndereco(this.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					this.getPessoaFisica().getEndereco().setEndereco(tmp);
					tmp = null;
				}
				tmp = cep.getBairro(this.getPessoaFisica().getEndereco().getCep());
				if (tmp != null) {
					System.out.println(tmp);
					this.getPessoaFisica().getEndereco()
							.setBairro(bairroRepository.buscarPorDescricao("bairro", tmp).get(0));
					tmp = null;
				}
			}
		} catch (IOException e) {
			System.out.println("Endereço não encontrado!");
			e.printStackTrace();
		}
	}

	private List<PessoaFisica> getPessoasFisicas() {
		pessoasFisica = pessoaRN.listarTodos(getNomePessoa());
		return pessoasFisica;
	}

	public PessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(PessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	public String getNomePessoa() {
		return nomePessoa;
	}

	public void setNomePessoa(String nomePessoa) {
		this.nomePessoa = nomePessoa;
	}

	public List<Sexo> listarSexos(String query) {
		return pessoaRN.listarSexos(query);
	}

	public List<RacaCor> listarRacasCor(String query) {
		return pessoaRN.listarRacasCor(query);
	}

	public List<Etnia> listarEtnias(String query) {
		return pessoaRN.listarEtnias(query);
	}

	public List<Municipio> listarMunicipios(String query) {
		return pessoaRN.listarMunicipios(query);
	}

	public List<Nacionalidade> listarNacionalidades(String query) {
		return pessoaRN.ListarNacionalidades(query);
	}

	public List<TipoLogradouro> listarTiposLogradouro(String query) {
		return pessoaRN.listarTipoLogradouros(query);
	}

	public List<Bairro> listarBairros(String query) {
		return pessoaRN.listarBairros(query);
	}

	public List<PessoaFisica> getPessoasFisica() {
		this.pessoasFisica = pessoaRN.listarTodos(getNomePessoa());
		return pessoasFisica;
	}
}
