package com.lmsystem.controller.cadastros.estoque;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Produto;
import com.lmsystem.modelo.SubGrupoProduto;
import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.services.cadastros.estoque.ProdutoRN;
import com.lmsystem.services.cadastros.estoque.SubGrupoProdutoRN;
import com.lmsystem.services.cadastros.estoque.UnidadeProdutoRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class ProdutoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Produto produto;

	private List<Produto> produtos;

	private String nomeproduto;

	@Inject
	protected ProdutoRN produtoRN;

	@Inject
	protected UnidadeProdutoRN unidadeProdutoRN;

	@Inject
	protected SubGrupoProdutoRN subGrupoProdutoRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeproduto("");
		pesquisar();
	}

	public void novo() {
		this.setProduto(new Produto());
	}

	public void salvar() {
		try {
			produtoRN.salvar(this.produto);
			this.produto = new Produto();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.produto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			produtoRN.remover(this.produto);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.produto = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "produto?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/estoque/produto", opcoes, null);
	}

	public void pesquisar() {
		this.getProdutos();
	}

	public Produto getProduto() {
		return produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	public List<SubGrupoProduto> listarSubgrupoProdutos(String query) {
		return subGrupoProdutoRN.listarTodos(query);
	}

	public List<UnidadeProduto> listarUnidadeProdutos(String query) {
		return unidadeProdutoRN.listarTodos(query);
	}

	public String getNomeproduto() {
		return nomeproduto;
	}

	public void setNomeproduto(String nomeproduto) {
		this.nomeproduto = nomeproduto;
	}

	public List<Produto> getProdutos() {
		this.produtos = produtoRN.listarTodos(getNomeproduto());
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

}
