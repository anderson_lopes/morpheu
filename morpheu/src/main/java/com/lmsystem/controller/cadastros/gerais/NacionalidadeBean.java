package com.lmsystem.controller.cadastros.gerais;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.Pais;
import com.lmsystem.services.cadastros.gerais.NacionalidadeRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class NacionalidadeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Nacionalidade nacionalidade;

	private String nomeNacionalidade;

	private List<Nacionalidade> nacionalidades;

	@Inject
	protected NacionalidadeRN nacionalidadeRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeNacionalidade("");
		pesquisar();
	}

	public void novo() {
		this.nacionalidade = new Nacionalidade();
	}

	public void salvar() {
		try {
			nacionalidadeRN.salvar(this.nacionalidade);
			this.nacionalidade = new Nacionalidade();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.nacionalidade = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			nacionalidadeRN.remover(this.nacionalidade);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.nacionalidade = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "nacionalidade?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/nacionalidade", opcoes, null);
	}

	public void pesquisar() {
		this.getNacionalidades();
	}

	public Nacionalidade getNacionalidade() {
		return nacionalidade;
	}

	public void setNacionalidade(Nacionalidade nacionalidade) {
		this.nacionalidade = nacionalidade;
	}

	public String getNomeNacionalidade() {
		return nomeNacionalidade;
	}

	public void setNomeNacionalidade(String nomeNacionalidade) {
		this.nomeNacionalidade = nomeNacionalidade;
	}

	public List<Nacionalidade> getNacionalidades() {
		this.nacionalidades = nacionalidadeRN.listarTodos(getNomeNacionalidade());
		return nacionalidades;
	}

	public List<Pais> listarPaises(String query) {
		return nacionalidadeRN.listarPaises(query);
	}

}
