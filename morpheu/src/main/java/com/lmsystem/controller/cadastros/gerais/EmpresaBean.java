package com.lmsystem.controller.cadastros.gerais;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Empresa;
import com.lmsystem.modelo.Uf;
import com.lmsystem.services.cadastros.gerais.EmpresaRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class EmpresaBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Empresa empresa;

	private String nomeEmpresa;

	private List<Empresa> empresas;

	@Inject
	protected EmpresaRN empresaRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeEmpresa("");
		pesquisar();
	}

	public void novo() {
		this.empresa = new Empresa();
	}

	public void salvar() {
		try {
			empresaRN.salvar(this.empresa);
			this.empresa = new Empresa();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.empresa = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
		}
	}

	public void remover() {
		try {
			empresaRN.remover(this.empresa);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.empresa = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
		}
	}

	public String limpar() {
		return "empresa?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/empresa", opcoes, null);
	}

	public void pesquisar() {
		this.getEmpresas();
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public String getNomeEmpresa() {
		return nomeEmpresa;
	}

	public void setNomeEmpresa(String nomeEmpresa) {
		this.nomeEmpresa = nomeEmpresa;
	}

	public List<Empresa> getEmpresas() {
		this.empresas = empresaRN.listarTodos(getNomeEmpresa());
		return empresas;
	}

	public List<Uf> listarUfs(String query) {
		return empresaRN.listarUfs(query);
	}

}
