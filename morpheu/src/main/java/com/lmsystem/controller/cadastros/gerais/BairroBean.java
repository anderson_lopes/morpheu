package com.lmsystem.controller.cadastros.gerais;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.services.cadastros.gerais.BairroRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class BairroBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Bairro bairro;

	private String nomeBairro;

	private List<Bairro> bairros;

	@Inject
	protected BairroRN bairroRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeBairro("");
		pesquisar();
	}

	public void novo() {
		this.bairro = new Bairro();
	}

	public void salvar() {
		try {
			bairroRN.salvar(this.bairro);
			this.bairro = new Bairro();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.bairro = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			bairroRN.remover(this.bairro);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.bairro = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "bairro?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/bairro", opcoes, null);
	}

	public void pesquisar() {
		this.getBairros();
	}

	public Bairro getBairro() {
		return bairro;
	}

	public void setBairro(Bairro bairro) {
		this.bairro = bairro;
	}

	public String getNomeBairro() {
		return nomeBairro;
	}

	public void setNomeBairro(String nomeBairro) {
		this.nomeBairro = nomeBairro;
	}

	public List<Bairro> getBairros() {
		this.bairros = bairroRN.listarTodos(getNomeBairro());
		return bairros;
	}

	public List<Municipio> listarMunicipios(String query) {
		return bairroRN.listarMunicipios(query);
	}

}
