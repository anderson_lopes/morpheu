package com.lmsystem.controller.cadastros.gerais;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Escolaridade;
import com.lmsystem.services.cadastros.gerais.EscolaridadeRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class EscolaridadeBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Escolaridade escolaridade;

	private String nomeEscolaridade;

	private List<Escolaridade> escolaridades;

	@Inject
	protected EscolaridadeRN escolaridadeRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeEscolaridade("");
		pesquisar();
	}

	public void novo() {
		this.escolaridade = new Escolaridade();
	}

	public void salvar() {
		try {
			escolaridadeRN.salvar(this.escolaridade);
			this.escolaridade = new Escolaridade();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.escolaridade = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			escolaridadeRN.remover(this.escolaridade);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.escolaridade = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "escolaridade?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/gerais/escolaridade", opcoes, null);
	}

	public void pesquisar() {
		this.getEscolaridades();
	}

	public Escolaridade getEscolaridade() {
		return escolaridade;
	}

	public void setEscolaridade(Escolaridade escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getNomeEscolaridade() {
		return nomeEscolaridade;
	}

	public void setNomeEscolaridade(String nomeEscolaridade) {
		this.nomeEscolaridade = nomeEscolaridade;
	}

	public List<Escolaridade> getEscolaridades() {
		this.escolaridades = escolaridadeRN.listarTodos(getNomeEscolaridade());
		return escolaridades;
	}

}
