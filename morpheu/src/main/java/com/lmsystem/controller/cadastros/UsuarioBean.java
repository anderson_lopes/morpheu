package com.lmsystem.controller.cadastros;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import com.lmsystem.modelo.Permissoes;
import com.lmsystem.modelo.Usuario;
import com.lmsystem.services.cadastros.UsuarioRN;
import com.lmsystem.util.FacesUtils;

@Named
@ViewScoped
public class UsuarioBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private Usuario usuario;

	private String nomeusuario;

	private List<Usuario> usuarios;

	@Inject
	protected UsuarioRN usuarioRN;

	@Inject
	protected FacesUtils facesUtils;

	public void iniciar() {
		this.setNomeusuario("");
		pesquisar();
	}

	public void novo() {
		this.usuario = new Usuario();
	}

	public void salvar() {
		try {
			usuarioRN.salvar(this.usuario);
			this.usuario = new Usuario();
			this.pesquisar();
			facesUtils.exibeSucesso("Registro cadastrado com sucesso!", null);
			this.usuario = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível inserir o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public void remover() {
		try {
			usuarioRN.remover(this.usuario);
			this.pesquisar();
			facesUtils.exibeSucesso("Registro removido com sucesso!", null);
			this.usuario = null;
		} catch (Exception e) {
			facesUtils.exibeErro("não foi possível remover o registro!", null);
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
		}
	}

	public String limpar() {
		return "usuario?faces-redirect=true";
	}

	public void openWindow() {
		Map<String, Object> opcoes = new HashMap<>();
		opcoes.put("responsive", true);
		RequestContext.getCurrentInstance().openDialog("/paginas/cadastros/usuario", opcoes, null);
	}
	
	public Permissoes[] getPermissoes() {
		return Permissoes.values();
	}

	public void pesquisar() {
		this.getUsuarios();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getNomeusuario() {
		return nomeusuario;
	}

	public void setNomeusuario(String nomeusuario) {
		this.nomeusuario = nomeusuario;
	}

	public List<Usuario> getUsuarios() {
		this.usuarios = usuarioRN.listarTodos(getNomeusuario());
		return usuarios;
	}

}
