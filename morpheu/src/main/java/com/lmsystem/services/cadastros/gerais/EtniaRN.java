package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Etnia;
import com.lmsystem.repository.EtniaRepository;
import com.lmsystem.util.Transacional;

public class EtniaRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected EtniaRepository etniaRepository;

	@Transacional
	public void salvar(Etnia etnia) {
		etniaRepository.guardar(etnia);
	}

	@Transacional
	public void remover(Etnia etnia) {
		etniaRepository.remover(etnia);
	}

	@Transacional
	public List<Etnia> listarTodos(String nomeEtnia) {
		if (!nomeEtnia.trim().equals("")) {
			return etniaRepository.buscarPorDescricao("etnia", nomeEtnia);
		}
		return etniaRepository.listarTodos();
	}

}
