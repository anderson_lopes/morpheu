package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Etnia;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.PessoaFisica;
import com.lmsystem.modelo.RacaCor;
import com.lmsystem.modelo.Sexo;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.repository.BairroRepository;
import com.lmsystem.repository.ContatoRepository;
import com.lmsystem.repository.EtniaRepository;
import com.lmsystem.repository.MunicipioRepository;
import com.lmsystem.repository.NacionalidadeRepository;
import com.lmsystem.repository.PessoaFisicaRepository;
import com.lmsystem.repository.RacaCorRepository;
import com.lmsystem.repository.SexoRepository;
import com.lmsystem.repository.TipoLogradouroRepository;
import com.lmsystem.util.Transacional;

public class PessoaFisicaRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected PessoaFisicaRepository pessoaFisicaRepository;

	@Inject
	protected ContatoRepository contatoRepository;

	@Inject
	protected SexoRepository sexoRepository;

	@Inject
	protected RacaCorRepository racaCorRepository;

	@Inject
	protected EtniaRepository etniaRepository;

	@Inject
	protected NacionalidadeRepository nacionalidadeRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected TipoLogradouroRepository tipoLogradouroRepository;

	@Inject
	protected BairroRepository bairroRepository;

	@Transacional
	public void salvar(PessoaFisica pessoaFisica) {
		if (pessoaFisica.getCpf().trim().equals("")) {
			pessoaFisica.setCpf(null);
		}
		if (pessoaFisica.getCns().trim().equals("")) {
			pessoaFisica.setCns(null);
		}
		pessoaFisicaRepository.guardar(pessoaFisica);
	}

	@Transacional
	public void remover(PessoaFisica pessoaFisica) {
		pessoaFisicaRepository.remover(pessoaFisica);
	}

	@Transacional
	public List<PessoaFisica> listarTodos(String nomePessoa) {
		if (!nomePessoa.trim().equals("")) {
			return pessoaFisicaRepository.buscarPorNomePessoa(nomePessoa);
		}
		return pessoaFisicaRepository.listarTodos();
	}

	public List<Sexo> listarSexos(String nomeSexo) {
		if (!nomeSexo.trim().equals("")) {
			return sexoRepository.buscarPorDescricao("sexo", nomeSexo);
		}
		return sexoRepository.listarTodos();
	}

	public List<RacaCor> listarRacasCor(String nomeRacaCor) {
		if (!nomeRacaCor.trim().equals("")) {
			return racaCorRepository.buscarPorDescricao("racacor", nomeRacaCor);
		}
		return racaCorRepository.listarTodos();
	}

	public List<Nacionalidade> ListarNacionalidades(String nomeNacionalidade) {
		if (!nomeNacionalidade.trim().equals("")) {
			return nacionalidadeRepository.buscarPorDescricao("nacionalidade", nomeNacionalidade);
		}
		return nacionalidadeRepository.listarTodos();
	}

	public List<Etnia> listarEtnias(String nomeEtnia) {
		if (!nomeEtnia.trim().equals("")) {
			return etniaRepository.buscarPorDescricao("etnia", nomeEtnia);
		}
		return etniaRepository.listarTodos();
	}

	public List<Municipio> listarMunicipios(String nomemunicipio) {
		if (!nomemunicipio.trim().equals("")) {
			return municipioRepository.buscarPorDescricao("municipio", nomemunicipio);
		}
		return municipioRepository.listarTodos();
	}

	public List<TipoLogradouro> listarTipoLogradouros(String nomeTipoLogradouro) {
		if (!nomeTipoLogradouro.trim().equals("")) {
			return tipoLogradouroRepository.buscarPorDescricao("tipologradouro", nomeTipoLogradouro);
		}
		return tipoLogradouroRepository.listarTodos();
	}

	public List<Bairro> listarBairros(String nomeBairro) {
		if (!nomeBairro.trim().equals("")) {
			return bairroRepository.buscarPorDescricao("bairro", nomeBairro);
		}
		return bairroRepository.listarTodos();
	}

}
