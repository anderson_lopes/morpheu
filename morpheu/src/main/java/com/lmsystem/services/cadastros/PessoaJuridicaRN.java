package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.PessoaJuridica;
import com.lmsystem.modelo.TipoLogradouro;
import com.lmsystem.repository.BairroRepository;
import com.lmsystem.repository.ContatoRepository;
import com.lmsystem.repository.MunicipioRepository;
import com.lmsystem.repository.PessoaJuridicaRepository;
import com.lmsystem.repository.TipoLogradouroRepository;
import com.lmsystem.util.Transacional;

public class PessoaJuridicaRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected PessoaJuridicaRepository pessoaJuridicaRepository;

	@Inject
	protected ContatoRepository contatoRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected TipoLogradouroRepository tipoLogradouroRepository;

	@Inject
	protected BairroRepository bairroRepository;

	@Transacional
	public void salvar(PessoaJuridica pessoaJuridica) {
		pessoaJuridicaRepository.guardar(pessoaJuridica);
	}

	@Transacional
	public void remover(PessoaJuridica pessoaJuridica) {
		pessoaJuridicaRepository.remover(pessoaJuridica);
	}

	@Transacional
	public List<PessoaJuridica> listarTodos(String nomefantasia) {
		if (!nomefantasia.trim().equals("")) {
			return pessoaJuridicaRepository.buscarPorDescricao("nomefantasia", nomefantasia);
		}
		return pessoaJuridicaRepository.listarTodos();
	}

	public List<Municipio> listarMunicipios(String nomemunicipio) {
		if (!nomemunicipio.trim().equals("")) {
			return municipioRepository.buscarPorDescricao("municipio", nomemunicipio);
		}
		return municipioRepository.listarTodos();
	}

	public List<TipoLogradouro> listarTipoLogradouros(String nomeTipoLogradouro) {
		if (!nomeTipoLogradouro.trim().equals("")) {
			return tipoLogradouroRepository.buscarPorDescricao("tipologradouro", nomeTipoLogradouro);
		}
		return tipoLogradouroRepository.listarTodos();
	}

	public List<Bairro> listarBairros(String nomeBairro) {
		if (!nomeBairro.trim().equals("")) {
			return bairroRepository.buscarPorDescricao("bairro", nomeBairro);
		}
		return bairroRepository.listarTodos();
	}

}
