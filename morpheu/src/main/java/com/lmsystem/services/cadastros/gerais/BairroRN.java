package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Bairro;
import com.lmsystem.modelo.Municipio;
import com.lmsystem.repository.BairroRepository;
import com.lmsystem.repository.MunicipioRepository;
import com.lmsystem.util.Transacional;

public class BairroRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected BairroRepository bairroRepository;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Transacional
	public void salvar(Bairro bairro) {
		bairroRepository.guardar(bairro);
	}

	@Transacional
	public void remover(Bairro bairro) {
		bairroRepository.remover(bairro);
	}

	@Transacional
	public List<Bairro> listarTodos(String nomeBairro) {
		if (!nomeBairro.trim().equals("")) {
			return bairroRepository.buscarPorDescricao("bairro", nomeBairro);
		}
		return bairroRepository.listarTodos();
	}

	@Transacional
	public List<Municipio> listarMunicipios(String municipio) {
		if (!municipio.trim().equals("")) {
			return municipioRepository.buscarPorDescricao("municipio", municipio);
		}
		return municipioRepository.listarTodos();
	}

}
