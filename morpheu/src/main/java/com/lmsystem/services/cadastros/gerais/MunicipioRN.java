package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Municipio;
import com.lmsystem.modelo.Uf;
import com.lmsystem.repository.MunicipioRepository;
import com.lmsystem.repository.UfRepository;
import com.lmsystem.util.Transacional;

public class MunicipioRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected MunicipioRepository municipioRepository;

	@Inject
	protected UfRepository ufRepository;

	@Transacional
	public void salvar(Municipio municipio) {
		municipioRepository.guardar(municipio);
	}

	@Transacional
	public void remover(Municipio municipio) {
		municipioRepository.remover(municipio);
	}

	@Transacional
	public List<Municipio> listarTodos(String nomeMunicipio) {
		if (!nomeMunicipio.trim().equals("")) {
			return municipioRepository.buscarPorDescricao("municipio", nomeMunicipio);
		}
		return municipioRepository.listarTodos();
	}

	@Transacional
	public List<Uf> listarUfs(String siglaUf) {
		if (!siglaUf.trim().equals("")) {
			return ufRepository.buscarPorDescricao("sigla", siglaUf);
		}
		return ufRepository.listarTodos();
	}

}
