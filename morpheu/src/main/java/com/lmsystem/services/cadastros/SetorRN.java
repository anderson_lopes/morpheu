package com.lmsystem.services.cadastros;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Setor;
import com.lmsystem.repository.SetorRepository;
import com.lmsystem.util.Transacional;

public class SetorRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected SetorRepository setorRepository;

	@Transacional
	public void salvar(Setor setor) {
		setorRepository.guardar(setor);
	}

	@Transacional
	public void remover(Setor setor) {
		setorRepository.remover(setor);
	}

	@Transacional
	public List<Setor> listarTodos(String nomeSetor) {
		if (!nomeSetor.trim().equals("")) {
			return setorRepository.buscarPorDescricao("setor", nomeSetor);
		}
		return setorRepository.listarTodos();
	}

}
