package com.lmsystem.services.cadastros.estoque;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.TipoPedido;
import com.lmsystem.repository.TipoPedidoRepository;
import com.lmsystem.util.Transacional;

public class TipoPedidoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private TipoPedidoRepository tipoPedidoRepository;

	@Transacional
	public void salvar(TipoPedido tipoPedido) {
		tipoPedidoRepository.guardar(tipoPedido);
	}

	@Transacional
	public void remover(TipoPedido tipoPedido) {
		tipoPedidoRepository.remover(tipoPedido);
	}

	@Transacional
	public List<TipoPedido> listarTodos(String nomeTipoPedido) {
		if (!nomeTipoPedido.trim().equals("")) {
			return tipoPedidoRepository.buscarPorDescricao("tipopedido", nomeTipoPedido);
		}
		return tipoPedidoRepository.listarTodos();
	}

}
