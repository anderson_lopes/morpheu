package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Nacionalidade;
import com.lmsystem.modelo.Pais;
import com.lmsystem.repository.NacionalidadeRepository;
import com.lmsystem.repository.PaisRepository;
import com.lmsystem.util.Transacional;

public class NacionalidadeRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected NacionalidadeRepository nacionalidadeRepository;

	@Inject
	protected PaisRepository paisRepository;

	@Transacional
	public void salvar(Nacionalidade nacionalidade) {
		nacionalidadeRepository.guardar(nacionalidade);
	}

	@Transacional
	public void remover(Nacionalidade nacionalidade) {
		nacionalidadeRepository.remover(nacionalidade);
	}

	@Transacional
	public List<Nacionalidade> listarTodos(String nomeNacionalidade) {
		if (!nomeNacionalidade.trim().equals("")) {
			return nacionalidadeRepository.buscarPorDescricao("nacionalidade", nomeNacionalidade);
		}
		return nacionalidadeRepository.listarTodos();
	}

	@Transacional
	public List<Pais> listarPaises(String nomePais) {
		if (!nomePais.trim().equals("")) {
			return paisRepository.buscarPorDescricao("pais", nomePais);
		}
		return paisRepository.listarTodos();
	}

}
