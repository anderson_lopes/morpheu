package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Operadora;
import com.lmsystem.repository.OperadoraRepository;
import com.lmsystem.util.Transacional;

public class OperadoraRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected OperadoraRepository operadoraRepository;

	@Transacional
	public void salvar(Operadora operadora) {
		operadoraRepository.guardar(operadora);
	}

	@Transacional
	public void remover(Operadora operadora) {
		operadoraRepository.remover(operadora);
	}

	@Transacional
	public List<Operadora> listarTodos(String nomeOperadora) {
		if (!nomeOperadora.trim().equals("")) {
			return operadoraRepository.buscarPorDescricao("operadora", nomeOperadora);
		}
		return operadoraRepository.listarTodos();
	}

}
