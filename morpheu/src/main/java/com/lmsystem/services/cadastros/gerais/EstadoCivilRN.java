package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.EstadoCivil;
import com.lmsystem.repository.EstadoCivilRepository;
import com.lmsystem.util.Transacional;

public class EstadoCivilRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected EstadoCivilRepository estadoCivilRepository;

	@Transacional
	public void salvar(EstadoCivil estadoCivil) {
		estadoCivilRepository.guardar(estadoCivil);
	}

	@Transacional
	public void remover(EstadoCivil estadoCivil) {
		estadoCivilRepository.remover(estadoCivil);
	}

	@Transacional
	public List<EstadoCivil> listarTodos(String nomeEstadoCivil) {
		if (!nomeEstadoCivil.trim().equals("")) {
			return estadoCivilRepository.buscarPorDescricao("estadocivil", nomeEstadoCivil);
		}
		return estadoCivilRepository.listarTodos();
	}

}
