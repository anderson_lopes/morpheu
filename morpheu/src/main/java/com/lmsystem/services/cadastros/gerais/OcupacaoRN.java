package com.lmsystem.services.cadastros.gerais;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.Ocupacao;
import com.lmsystem.repository.OcupacaoRepository;
import com.lmsystem.util.Transacional;

public class OcupacaoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected OcupacaoRepository ocupacaoRepository;

	@Transacional
	public void salvar(Ocupacao ocupacao) {
		ocupacaoRepository.guardar(ocupacao);
	}

	@Transacional
	public void remover(Ocupacao ocupacao) {
		ocupacaoRepository.remover(ocupacao);
	}

	@Transacional
	public List<Ocupacao> listarTodos(String nomeOcupacao) {
		if (!nomeOcupacao.trim().equals("")) {
			return ocupacaoRepository.buscarPorDescricao("ocupacao", nomeOcupacao);
		}
		return ocupacaoRepository.listarTodos();
	}

}
