package com.lmsystem.services.cadastros.estoque;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.GrupoProduto;
import com.lmsystem.repository.GrupoProdutoRepository;
import com.lmsystem.util.Transacional;

public class GrupoProdutoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected GrupoProdutoRepository grupoProdutoRepository;

	@Transacional
	public void salvar(GrupoProduto grupoProduto) {
		grupoProdutoRepository.guardar(grupoProduto);
	}

	@Transacional
	public void remover(GrupoProduto grupoProduto) {
		grupoProdutoRepository.remover(grupoProduto);
	}

	@Transacional
	public List<GrupoProduto> listarTodos(String nomeGrupoProdutos) {
		if (!nomeGrupoProdutos.trim().equals("")) {
			return grupoProdutoRepository.buscarPorDescricao("grupoproduto", nomeGrupoProdutos);
		}
		return grupoProdutoRepository.listarTodos();
	}

}
