package com.lmsystem.services.cadastros.estoque;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import com.lmsystem.modelo.SubGrupoProduto;
import com.lmsystem.repository.SubGrupoProdutoRepository;
import com.lmsystem.util.Transacional;

public class SubGrupoProdutoRN implements Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	protected SubGrupoProdutoRepository subGrupoProdutoRepository;

	@Transacional
	public void salvar(SubGrupoProduto subGrupoProduto) {
		subGrupoProdutoRepository.guardar(subGrupoProduto);
	}

	@Transacional
	public void remover(SubGrupoProduto subGrupoProduto) {
		subGrupoProdutoRepository.remover(subGrupoProduto);
	}

	@Transacional
	public List<SubGrupoProduto> listarTodos(String nomeSubGrupoProdutos) {
		if (!nomeSubGrupoProdutos.trim().equals("")) {
			return subGrupoProdutoRepository.buscarPorDescricao("subgrupo", nomeSubGrupoProdutos);
		}
		return subGrupoProdutoRepository.listarTodos();
	}

}
