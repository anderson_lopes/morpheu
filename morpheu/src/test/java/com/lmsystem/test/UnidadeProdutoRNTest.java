package com.lmsystem.test;

import javax.persistence.EntityManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.lmsystem.modelo.UnidadeProduto;
import com.lmsystem.util.EntityManagerProducer;

public class UnidadeProdutoRNTest {

	private EntityManagerProducer entityManagerProducer;
	private EntityManager em;
	private UnidadeProduto unidadeProduto;

	@Before
	public void inicia() {
		entityManagerProducer = new EntityManagerProducer();
		em = entityManagerProducer.createEntityManager();
		unidadeProduto = new UnidadeProduto();
	}

	@Test
	public void deveGuardarUnidadeProduto() {

		unidadeProduto.setFator(1);
		unidadeProduto.setSigla("UND");
		unidadeProduto.setUnidade("UNIDADE");

		em.getTransaction().begin();
		em.persist(unidadeProduto);
		em.getTransaction().commit();

	}

	@Test
	public void deveremoverUnidadeProduto() {
		em.getTransaction().begin();
		em.remove(unidadeProduto);
		em.getTransaction().commit();

	}

	@After
	public void finaliza() {
		entityManagerProducer.closeEntityManager(em);
	}

}
