package com.lmsystem.test;

import java.io.IOException;
import java.text.ParseException;

import org.junit.Assert;
import org.junit.Test;

import com.lmsystem.util.BuscaCEP;

public class BuscaCepTest {

	BuscaCEP buscaCEP = new BuscaCEP();

	@Test
	public void deveRetornarEnderecoValidoTest() {
		try {
			Assert.assertEquals("Rua Dizinha Rodrigues", buscaCEP.getEndereco("59621680"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarEnderecoNuloTest() {
		try {
			Assert.assertEquals(null, buscaCEP.getEndereco(""));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarBairroValidoTest() {
		try {
			Assert.assertEquals("Santo Antônio", buscaCEP.getBairro("59621680"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarBairroNuloTest() {
		try {
			Assert.assertEquals(null, buscaCEP.getBairro(""));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarCidadeValidoTest() {
		try {
			Assert.assertEquals("Mossoró", buscaCEP.getCidade("59621680"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarCidadeNuloTest() {
		try {
			Assert.assertEquals(null, buscaCEP.getCidade(""));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarUFValidoTest() {
		try {
			Assert.assertEquals("RN", buscaCEP.getUF("59621680"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarUFNuloTest() {
		try {
			Assert.assertEquals(null, buscaCEP.getUF(""));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void deveRetornarLatitudeLogitudeValidoTest() {
		try {
			Assert.assertEquals("-5.1604412,-37.3398768", buscaCEP.getLatLong("59621680"));
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	@Test(expected = ArrayIndexOutOfBoundsException.class)
	public void deveRetornarExcecaoTest() {
		try {
			buscaCEP.getLatLong("");
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

}
