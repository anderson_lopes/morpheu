package com.lmsystem.test;

import javax.persistence.EntityManager;

import org.junit.Ignore;
import org.junit.Test;

import com.lmsystem.util.EntityManagerProducer;

public class EntityManagerProducerTest {

	@Test
	@Ignore
	public void gerEntityManager() {
		EntityManagerProducer entityManagerProducer = new EntityManagerProducer();
		EntityManager entityManager = entityManagerProducer.createEntityManager();
		entityManagerProducer.closeEntityManager(entityManager);
	}

}
